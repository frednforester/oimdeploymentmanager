/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager;

//import static deployment.manager.Exporter.CONFIGFILE;
import static deployment.manager.Exporter.CONFIGFILE;
import deployment.manager.utils.OIMHelperClient;
import deployment.manager.utils.WebConfigLoader;
import java.util.Properties;
import java.util.Scanner;
import javax.naming.Context;

/**
 *
 * @author fforester
 */
public class BaseDeployer extends OIMHelperClient {
    
    
    protected void authenticate(Properties configProperties) throws Exception
    {
        
        Scanner inputReader = new Scanner(System.in);
        
        System.out.println("Please enter OIM Admin Password");
        String passwd = inputReader.nextLine();
        //logger.debug("Name:" + passwd);
        
        this.setOIMInitialContextFactory(configProperties.getProperty(Context.INITIAL_CONTEXT_FACTORY));
        this.setOIMURL(configProperties.getProperty(Context.PROVIDER_URL));
        this.setOIMUserName(configProperties.getProperty(Context.SECURITY_PRINCIPAL));
        this.setXlAuthLogin(configProperties.getProperty("java.security.auth.login.config"));
        this.setXlHomeDir(configProperties.getProperty("xl.HomeDir"));
        this.setOIMPassword(passwd);
        try
        {
            this.loginWithCustomEnv();
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    
    protected Properties getExportConnection(Properties configProperties)
    {
        Properties props = new Properties();
        
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY,configProperties.getProperty(Context.INITIAL_CONTEXT_FACTORY));
        props.setProperty(Context.PROVIDER_URL,configProperties.getProperty("export." +Context.PROVIDER_URL));
        props.setProperty(Context.SECURITY_PRINCIPAL,configProperties.getProperty("export." +Context.SECURITY_PRINCIPAL));
        props.setProperty("java.security.auth.login.config",configProperties.getProperty("java.security.auth.login.config"));
        props.setProperty("xl.HomeDir",configProperties.getProperty("xl.HomeDir"));
        return props;
        
    }
    
    protected Properties getImportConnection(Properties configProperties)
    {
        Properties props = new Properties();
        
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY,configProperties.getProperty(Context.INITIAL_CONTEXT_FACTORY));
        props.setProperty(Context.PROVIDER_URL,configProperties.getProperty("import." +Context.PROVIDER_URL));
        props.setProperty(Context.SECURITY_PRINCIPAL,configProperties.getProperty("import." +Context.SECURITY_PRINCIPAL));
        props.setProperty("java.security.auth.login.config",configProperties.getProperty("java.security.auth.login.config"));
        props.setProperty("xl.HomeDir",configProperties.getProperty("xl.HomeDir"));
        return props;
    }
    
    
    protected Properties getConfigFile() throws Exception
    {
        // load config from classpath
        WebConfigLoader loader = new WebConfigLoader();
        try
        {
            loader.getConfig(CONFIGFILE);
        }
        catch(Exception e)
        {
            //logger.error("Config FIle:" + BulkExporter.CONFIGFILE,e);
            throw e;
        }
        // get the config as props
        Properties props = loader.getConfigProps();
        if (props == null || props.isEmpty())
        {
            throw new Exception("Config File is Empty");
        }
        return props;
    }
    
}
