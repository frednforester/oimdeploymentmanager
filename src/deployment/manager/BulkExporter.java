/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager;

import deployment.manager.utils.DMHelper;
import deployment.manager.utils.WebConfigLoader;
import java.io.File;
import java.util.Collection;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author fforester
 * Exports in bulk the resources from a single OIM instance. the ExportCategories.properties file lists the categories to export.
 * all the resources in that category will be exported in separate files using the resource name into a directory 
 * where the directory name will match the category. see the example ExportCategories.properties. it is set up to export almost all of the
 * resource types in OIM. others can be added to list as needed. some resources are bundled together like jobs and tasks. Resource Objects and
 * Process form Definitions. the main config file config.properties needs to be in the classpath.
 */
public class BulkExporter extends BaseDeployer {
    
    
    public static final String CONFIGFILE="config.properties";
    private Logger logger = Logger.getLogger(BulkExporter.class);
    private Properties configProperties;
    private Properties exportCategories;

    private DMHelper dmHelper;
    
    public static void main(String[] args)
    {
        
        BulkExporter be = new BulkExporter();
        be.newMain();
        
    }
    
    private void newMain()
    {
        
        try
        {
            validate();
        }
        catch(Exception e)
        {
            logger.error("Validation failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            Properties exportProps = this.getExportConnection(this.configProperties);
            System.out.println("" + exportProps);
            this.authenticate(exportProps);
        }
        catch(Exception e)
        {
            logger.error("OIM Auth Failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            dmHelper = new DMHelper(this.getClient());
            dmHelper.setBaseDir(configProperties.getProperty("ExportDirectoryPath"));
            startExport();
        }
        catch(Exception e)
        {
            logger.error("Error in Export:" + e.getMessage(),e);
        }
        
    }
    
    private void startExport() throws Exception {
        
        try {
            
            String printNames = exportCategories.getProperty("printCategories");
            if (StringUtils.isBlank(printNames)) {
                printNames = "false";
            }
            if (printNames.equalsIgnoreCase("y") || printNames.equalsIgnoreCase("true")) {
                Collection<String> cats = dmHelper.retrieveCategories();
                for(String ro : cats)
                {
                    System.out.println("Cat:" + ro);
                }
                return;
            }
            
            
            String exportMeta = exportCategories.getProperty("exportUserMetaData");
            if (StringUtils.isBlank(exportMeta)) {
                exportMeta = "false";
            }
            if (exportMeta.equalsIgnoreCase("y") || exportMeta.equalsIgnoreCase("true")) {
                dmHelper.exportUserMetadata();
            }

            String tmp = exportCategories.getProperty("categories");
            String[] categories = null;
            if (!StringUtils.isBlank(tmp)) {
                categories = tmp.split(",");
            }

            if (categories != null && categories.length > 0) {
                dmHelper.exportBasicCategories(categories);
            }

            String exportJobs = exportCategories.getProperty("exportJobs");
            if (StringUtils.isBlank(exportJobs)) {
                exportJobs = "false";
            }
            if (exportJobs.equalsIgnoreCase("y") || exportJobs.equalsIgnoreCase("true")) {
                dmHelper.exportAllJobs();
            }

            String exportProcessDefs = exportCategories.getProperty("exportProcessDefs");
            if (StringUtils.isBlank(exportProcessDefs)) {
                exportProcessDefs = "false";
            }
            if (exportProcessDefs.equalsIgnoreCase("y") || exportProcessDefs.equalsIgnoreCase("true")) {
                dmHelper.exportAllProcesses();
            }

            String exportProcessForms = exportCategories.getProperty("exportProcessForms");
            if (StringUtils.isBlank(exportProcessForms)) {
                exportProcessForms = "false";
            }
            if (exportProcessForms.equalsIgnoreCase("y") || exportProcessForms.equalsIgnoreCase("true")) {
                dmHelper.exportAllProcessForms();
            }
        } catch (Exception e) {
            throw e;
        }

    }
    
    private void validate() throws Exception
    {
        try
        {
            this.configProperties = this.getConfigFile();
        }
        catch(Exception e)
        {
            throw e;
        }
        logger.debug("Properties:" + configProperties);
        
        String tmp = configProperties.getProperty("ExportCategoryFilePath");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid ExportCategoryFilePath");
        }
        
        try
        {
            File file = new File(tmp);
            if (!file.exists())
            {
                throw new Exception("ExportCategoryFilePath does not exist");
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        tmp = configProperties.getProperty("ExportDirectoryPath");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid ExportDirectoryPath");
        }
        
        try
        {
            File file = new File(tmp);
            if (!file.exists())
            {
                throw new Exception("ExportDirectoryPath does not exist");
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        
        try
        {
            this.loadExportCategories();
        }
        catch(Exception e)
        {
            throw e;
        }
        
    }
    
    public void showCategoryNames() throws Exception
    {
        try
        {
            Collection<String> cats = this.dmHelper.retrieveCategories();
            logger.debug("cats:" + cats);
            for(String ro : cats)
            {
                System.out.println("Cat:" + ro);
            }
        }
        catch(Exception e)
        {
            logger.error("APIError:" + e.getMessage(),e);
            throw e;
        }
    }
    
    private void loadExportCategories() throws Exception
    {
        WebConfigLoader loader = new WebConfigLoader();
        try
        {
            loader.getConfig(configProperties.getProperty("ExportCategoryFilePath"));
        }
        catch(Exception e)
        {
            logger.error("ExportCategoryFilePath Failed to load");
            throw e;
        }
        
        this.exportCategories = loader.getConfigProps();
        logger.debug("Properties:" + exportCategories);
        
    }
}
