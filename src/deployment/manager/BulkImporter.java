/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager;

import deployment.manager.utils.DMHelper;
import deployment.manager.utils.WebConfigLoader;
import java.io.File;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author fforester
 * imports the resources exported from the bulk exporter. it will expect the same directory structure where the Directory name matches the category name.
 * 
 */
public class BulkImporter extends BaseDeployer {
    
    
    public static final String CONFIGFILE="config.properties";
    private Logger logger = Logger.getLogger(BulkImporter.class);
    private Properties configProperties;
    private Properties importCategories;
    
    private DMHelper dmHelper;
    
    
    public static void main(String[] args)
    {
        
        BulkImporter bi = new BulkImporter();
        bi.newMain();
        
    }
    
    private void newMain()
    {
        
        try
        {
            validate();
        }
        catch(Exception e)
        {
            logger.error("Validation failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            Properties importProps = this.getImportConnection(this.configProperties);
            this.authenticate(importProps);
        }
        catch(Exception e)
        {
            logger.error("OIM Auth Failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            dmHelper = new DMHelper(this.getClient());
            dmHelper.setBaseDir(configProperties.getProperty("ExportDirectoryPath"));
            
            startImport();
        }
        catch(Exception e)
        {
            logger.error("Error in Import:" + e.getMessage(),e);
        }
        
    }
    
    private void startImport() throws Exception
    {
        
        try
        {
            dmHelper.getLock();
        }
        catch(Exception e)
        {
            throw e;
        }
        String tmp = importCategories.getProperty("categories");
        String[] categories = null;
        if (!StringUtils.isBlank(tmp))
        {
            categories = tmp.split(",");
        }
        
        try {
            if (categories != null && categories.length > 0) {
                dmHelper.importBasicCategories(categories);
            }
        } catch (Exception e) {
            throw e;
        }

        
        
    }
    
    
    private void validate() throws Exception
    {
        try
        {
            this.configProperties = this.getConfigFile();
        }
        catch(Exception e)
        {
            throw e;
        }
        logger.debug("Properties:" + configProperties);
        
        String tmp = configProperties.getProperty("ImportCategoryFilePath");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid ImportCategoryFilePath");
        }
        
        try
        {
            File file = new File(tmp);
            if (!file.exists())
            {
                throw new Exception("ImportCategoryFilePath does not exist");
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        tmp = configProperties.getProperty("ImportDirectoryPath");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid ImportDirectoryPath");
        }
        
        try
        {
            File file = new File(tmp);
            if (!file.exists())
            {
                throw new Exception("ImportDirectoryPath does not exist");
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        
        try
        {
            this.loadImportCategories();
        }
        catch(Exception e)
        {
            throw e;
        }
        
    }
    
    
    private void loadImportCategories() throws Exception
    {
        WebConfigLoader loader = new WebConfigLoader();
        try
        {
            loader.getConfig(configProperties.getProperty("ImportCategoryFilePath"));
        }
        catch(Exception e)
        {
            logger.error("ImportCategoryFilePath Failed to load");
            throw e;
        }
        
        this.importCategories = loader.getConfigProps();
        logger.debug("Properties:" + importCategories);
        
    }
}
