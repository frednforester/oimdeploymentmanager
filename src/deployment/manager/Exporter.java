/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager;

import com.thortech.xl.vo.ddm.RootObject;
import deployment.manager.utils.ArgsParser;
import deployment.manager.utils.DMHelper;
import deployment.manager.utils.WebConfigLoader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author fforester
 * * Basic Connector exporter. The resource categories are listed in the ConnectorExportConfigFile. it looks like an old windows
 * ini file where you specify a resource type then a list of names for the resources of that type you want exported. the types should match the
 * OIM Category names for those resources. see the example ExportConnector.properties
 * the main config file config.properties needs to be in the classpath.
 */
public class Exporter extends BaseDeployer {
    
    public static final String CONFIGFILE="config.properties";
    private Logger logger = Logger.getLogger(Exporter.class);
    private Properties configProperties;
    private INIConfiguration iniConfig;
    private final String[] requiredArgs = {"-exportConfigFile"};
    private String configFileName;
    private String exportFileName;
    private String exportDirectory;
    //private OIMDeploymentManager dmManager;
    private DMHelper dmHelper;
    
    public static void main(String[] args)
    {
        
        Exporter exp = new Exporter();
        exp.newMain(args);
        
    }
    
    private void newMain(String[] args)
    {
        try
        {
            validate(args);
        }
        catch(Exception e)
        {
            logger.error("Validation failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            Properties exportProps = this.getExportConnection(this.configProperties);
            this.authenticate(exportProps);
        }
        catch(Exception e)
        {
            logger.error("OIM Auth Failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            //dmManager = new OIMDeploymentManager(this.getClient());
            dmHelper = new DMHelper(this.getClient());
            dmHelper.setBaseDir(this.exportDirectory);
            startExport();
        }
        catch(Exception e)
        {
            logger.error("Error in Export:" + e.getMessage(),e);
        }
    }
    
    private void startExport() throws Exception 
    {
        Set<String> sections = this.iniConfig.getSections();
        Collection<RootObject> exportSet = new HashSet<RootObject>();
        Collection<RootObject> cleanSet = new HashSet<RootObject>();
        // process all the ini sections and the resources specified in those sections.
        try {
            for (String s : sections) {
                logger.debug("Section:" + s);
                // empty section
                if (s == null)
                    continue;
                if ("ExportFile".equalsIgnoreCase(s))
                    continue;
                SubnodeConfiguration subConfig = this.iniConfig.getSection(s);
                Iterator i = subConfig.getKeys();
                while (i.hasNext()) {
                    String resource = (String) i.next();
                    if (resource != null)
                    {
                        resource = resource.replace("..",".");
                    }
                    logger.debug("Key:" + resource);
                    exportSet.addAll(dmHelper.getRootObject(s, resource));
                }
            }

            logger.debug(exportSet.size());
            if (exportSet.size() > 0) {
                exportSet = dmHelper.retrieveDependencyTree(exportSet);
                dmHelper.exportObjects(exportSet, this.exportFileName);
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    private void validate(String[] args) throws Exception
    {
        Map argMap = null;
        try
        {
            argMap = ArgsParser.getArgs(args, requiredArgs);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        try
        {
            this.configProperties = this.getConfigFile();
        }
        catch(Exception e)
        {
            throw e;
        }
        logger.debug("Properties:" + configProperties);
        
        String tmp = (String)argMap.get("-exportConfigFile");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid exportConfigFile");
        }
        
        try
        {
            File file = new File(tmp);
            if (!file.exists())
            {
                throw new Exception("exportConfigFile does not exist");
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        this.configFileName = tmp;
        
        // load the ini file.
        try
        {
            this.loadExportCategories();
        }
        catch(Exception e)
        {
            throw e;
        }
        
        SubnodeConfiguration expFileName = iniConfig.getSection("ExportFile");
        tmp = expFileName.getString("FileName");
        logger.debug("FIlename:" + tmp);
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid ExportFile FileName");
        }
        
        try
        {
            File file = new File(tmp);
            if (file.isDirectory())
            {
                throw new Exception("ExportFile FileName is not a file name");
            }
            
            logger.debug("Name:" + file.getName());
            //logger.debug("Path:" + file.getAbsoluteFile().getParentFile());
            logger.debug("Path:" + file.getAbsoluteFile().getParent());
            this.exportFileName = file.getName();
            this.exportDirectory = file.getAbsoluteFile().getParent();
        }
        catch(Exception e)
        {
            throw e;
        }
        
    }
    
    /**
     * load the ini file with the categories and the resources to export
     * @throws Exception 
     */
    private void loadExportCategories() throws Exception
    {
        WebConfigLoader loader = new WebConfigLoader();
        iniConfig = new INIConfiguration();
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(this.configFileName));
            iniConfig.read(reader);
        }
        catch(Exception e)
        {
            logger.error("exportConfigFile Failed to load");
            throw e;
        }
        
        
        
        
    }
    
    
}
