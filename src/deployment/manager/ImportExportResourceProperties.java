/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package deployment.manager;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.log4j.Logger;
import deployment.manager.utils.OIMITResources;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class ImportExportResourceProperties extends BaseDeployer {


    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Properties configProperties;
    
    private String operation;
    private String fileName;
    
    private OIMITResources itres;

    public static void main(String[] args)
    {
        ImportExportResourceProperties ierp = new ImportExportResourceProperties();
        ierp.newMain();
    }
    
    
    private void newMain() {

        try {
            validate();
        } 
        catch (Exception e) 
        {
            logger.error("Validation failed:" + e.getMessage(), e);
            return;
        }
        
        Properties connectProps = null;
        if (this.operation.equalsIgnoreCase("exportfromsource"))
        {
            connectProps = this.getExportConnection(this.configProperties);
            this.operation = "export";
        }
        else
        if (this.operation.equalsIgnoreCase("exportfromtarget"))
        {
            connectProps = this.getImportConnection(this.configProperties);
            this.operation = "export";
        }
        else
        if (this.operation.equalsIgnoreCase("import"))
        {
            connectProps = this.getImportConnection(this.configProperties);
        }
        else
        {
            logger.error("Invalid Operation");
            return;
        }
        

        try {
            this.authenticate(connectProps);
        } catch (Exception e) {
            logger.error("OIM Auth Failed:" + e.getMessage(), e);
            return;
        }
        
        try
        {
            itres = new OIMITResources(this.getClient());
            startImportExport();
        }
        catch(Exception e)
        {
            logger.error("Error in Import:" + e.getMessage(),e);
        }
    }
    
    private void startImportExport() throws Exception
    {
        if (this.operation.equalsIgnoreCase("export"))
            this.writeITResourceValues();
        
        if (this.operation.equalsIgnoreCase("import"))
            this.loadITResourceValues();
        
    }

    private void validate() throws Exception
    {
        try
        {
            this.configProperties = this.getConfigFile();
        }
        catch(Exception e)
        {
            throw e;
        }
        logger.debug("Properties:" + configProperties);
        
        String tmp = configProperties.getProperty("ITResourceImpExpOperation");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid ITResourceImpExpOperation");
        }
        if (!tmp.equalsIgnoreCase("exportfromsource") && !tmp.equalsIgnoreCase("exportfromtarget") && !tmp.equalsIgnoreCase("import"))
        {
            throw new Exception("Invalid ITResourceImpExpOperation");
        }
        
        this.operation = tmp;
        tmp = configProperties.getProperty("ITResourceImpExpFileName");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid ITResourceImpExpFileName");
        }
        this.fileName = tmp;
        
        if (this.operation.equalsIgnoreCase("import"))
        {
            try
            {
                File file = new File(this.fileName);
                if (!file.exists())
                {
                    throw new Exception("ITResourceImpExpFileName does not exist");
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }

    
    public void writeITResourceValues() throws Exception
    {
        BufferedWriter fout = null;
        try {
            
            Writer writer = new OutputStreamWriter(new FileOutputStream(this.fileName), "UTF-8");
            fout = new BufferedWriter(writer);
            Map allRes = itres.getAllITResources();
            Set<String> keys = allRes.keySet();
            for(String key : keys)
            {
                long lKey = new Long(key).longValue();
                String val = (String)allRes.get(key);
                List<String> records = itres.printResourceParms(lKey, val);
                for(String record : records)
                {
                    fout.write(record);
                    fout.newLine();
                }
            }

        } catch (Exception e) {
            throw e;
        }
        finally
        {
            if (fout != null)
            {
                fout.flush();
                fout.close();
            }
        }
    }

    public void loadITResourceValues() throws Exception
    {
        Map<String,Map> resMap = new HashMap<String,Map>();
        Properties itresProps = new Properties();
        try
        {
            Reader reader = new InputStreamReader(new FileInputStream(this.fileName),"UTF-8");
            itresProps.load(reader);
            
        }
        catch(Exception e)
        {
            throw e;
        }

        Enumeration propKeys = itresProps.propertyNames();
        while(propKeys.hasMoreElements())
        {
            String key = (String)propKeys.nextElement();
            logger.debug("Map Key " + key);
            String[] keySplit = key.split("\\.");
            logger.debug("Map Key Length " + keySplit.length);
            String resKey = keySplit[1];
            String resName = keySplit[0];
            Map itresParms = resMap.get(resName);
            if (itresParms == null)
            {
                itresParms = new HashMap<String,String>();
            }
            itresParms.put(resKey, itresProps.get(key));
            resMap.put(resName, itresParms);
        }
        
        try
        {
            processResources(resMap);
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public void processResources(Map<String,Map> resMaps) throws Exception
    {
        Set<String> keys = resMaps.keySet();

        for(String resName : keys)
        {
            Map resParms = resMaps.get(resName);
            logger.debug("Resource " + resName + " Properties " + resParms);
            try
            {
                long resKey = itres.getItResource(resName);
                if (resKey == 0l)
                {
                    logger.error("Could not find resource " + resName);
                }
                itres.updateITResource(resKey, resParms);
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }

}
