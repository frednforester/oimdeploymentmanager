/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager;

import com.thortech.xl.vo.ddm.RootObject;
import deployment.manager.utils.ArgsParser;
import deployment.manager.utils.DMHelper;
import deployment.manager.utils.WebConfigLoader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author fforester
 * Basic File importer. the files are listed in the ConnectorImportConfigFile. it looks like an old windows
 * ini file with a [FileList] section. see the example ImportConnector.properties
 * the main config file config.properties needs to be in the classpath
 */
public class Importer extends BaseDeployer {
    
    public static final String CONFIGFILE="config.properties";
    private final Logger logger = Logger.getLogger(Importer.class);
    private Properties configProperties;
    private INIConfiguration iniConfig;

    private final String[] requiredArgs = {"-importConfigFile"};
    private String importConfigFileName;
    
    private DMHelper dmHelper;
    public static void main(String[] args)
    {
        
        Importer imp = new Importer();
        imp.newMain(args);
        
    }
    
    private void newMain(String[] args)
    {
        try
        {
            validate(args);
        }
        catch(Exception e)
        {
            logger.error("Validation failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            Properties importProps = this.getImportConnection(this.configProperties);
            this.authenticate(importProps);
        }
        catch(Exception e)
        {
            logger.error("OIM Auth Failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            //dmManager = new OIMDeploymentManager(this.getClient());
            dmHelper = new DMHelper(this.getClient());
            dmHelper.setBaseDir(configProperties.getProperty("ConnectorImportDirectoryPath"));
            startImport();
        }
        catch(Exception e)
        {
            logger.error("Error in Export:" + e.getMessage(),e);
        }
    }
    
    private void startImport() throws Exception 
    {
        try
        {
            dmHelper.getLock();
        }
        catch(Exception e)
        {
            throw e;
        }
        
        Set<String> sections = this.iniConfig.getSections();
        Set<RootObject> exportSet = new HashSet<RootObject>();
        SubnodeConfiguration subConfig = this.iniConfig.getSection("FileList");
        
        try {
            Iterator i = subConfig.getKeys();
            while (i.hasNext()) {
                String fileName = (String) i.next();
                if (fileName != null)
                {
                    fileName = fileName.replace("..",".");
                }
                logger.debug("File:" + fileName);
                dmHelper.importFile(fileName);
            }
            
        } catch (Exception e) {
            throw e;
        }
    }
    
    private void validate(String[] args) throws Exception
    {
        Map argMap = null;
        try
        {
            argMap = ArgsParser.getArgs(args, requiredArgs);
        }
        catch(Exception e)
        {
            throw e;
        }
        try
        {
            this.configProperties = this.getConfigFile();
        }
        catch(Exception e)
        {
            throw e;
        }
        
        logger.debug("Properties:" + configProperties);
        logger.debug("Args:" + argMap);
        String tmp = (String)argMap.get("-importConfigFile");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid importConfigFile");
        }
        
        try
        {
            File file = new File(tmp);
            if (!file.exists())
            {
                throw new Exception("importConfigFile does not exist");
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        
        this.importConfigFileName = tmp;
        
        try
        {
            this.loadImportCategories();
        }
        catch(Exception e)
        {
            throw e;
        }
        
    }
    
    
    private void loadImportCategories() throws Exception
    {
        WebConfigLoader loader = new WebConfigLoader();
        iniConfig = new INIConfiguration();
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(this.importConfigFileName));
            iniConfig.read(reader);
        }
        catch(Exception e)
        {
            logger.error("importConfigFile Failed to load");
            throw e;
        }
        
    }
    
}
