/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager;

import deployment.manager.utils.ArgsParser;
import deployment.manager.utils.JarLoader;
import deployment.manager.utils.PluginLoader;
import deployment.manager.utils.WebConfigLoader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author fforester
 */
public class LoadBinaries extends BaseDeployer {
    
    
    public static final String CONFIGFILE="config.properties";
    private Logger logger = Logger.getLogger(LoadBinaries.class);
    private Properties configProperties;
    private INIConfiguration iniConfig;
    private final String[] requiredArgs = {"-binaryConfigFile"};
    private String configFileName;

    private JarLoader jarLoader;
    private PluginLoader pluginLoader;
    
    private List<String> jarTypes = Arrays.asList(new String[]{"JavaTasks","ScheduleTask","ICFBundle","ThirdParty"});
    
    public static void main(String[] args)
    {
        
        LoadBinaries lbin = new LoadBinaries();
        lbin.newMain(args);
        
    }
    
    private void newMain(String[] args)
    {
        try
        {
            validate(args);
        }
        catch(Exception e)
        {
            logger.error("Validation failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            Properties importProps = this.getImportConnection(this.configProperties);
            this.authenticate(importProps);
        }
        catch(Exception e)
        {
            logger.error("OIM Auth Failed:" + e.getMessage(),e);
            return;
        }
        
        try
        {
            jarLoader = new JarLoader(this.getClient());
            pluginLoader = new PluginLoader(this.getClient());
            startExport();
        }
        catch(Exception e)
        {
            logger.error("Error in Export:" + e.getMessage(),e);
        }
    }
    
    private void startExport() throws Exception 
    {
        try
        {
            loadJars();
            loadPlugins();
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    private void loadJars() throws Exception 
    {
        Set<String> sections = this.iniConfig.getSections();
        // process all the ini sections and the resources specified in those sections.
        try {
            for (String s : sections) {
                logger.debug("Section:" + s);
                // empty section
                if (s == null)
                    continue;
                if (!jarTypes.contains(s))
                    continue;
                SubnodeConfiguration subConfig = this.iniConfig.getSection(s);
                Iterator i = subConfig.getKeys();
                while (i.hasNext()) {
                    String resource = (String) i.next();
                    
                    if (resource != null)
                    {
                        resource = resource.replace("..",".");
                    }
                    /* the files are on unix
                    File resourceFile = new File(resource);
                    if (!resourceFile.exists())
                    {
                        logger.error("File does not exist:" + resource);
                        continue;
                    }
                    */
                    String jarName = resource;
                    resource = resource.replace("\\", "/");
                    int index = resource.lastIndexOf("/");
                    if (index > 0)
                    {
                        jarName = resource.substring(index+1);
                    }
                    logger.debug("Load:" + s + ":" + resource + ":" + jarName);
                    jarLoader.loadJar(s,resource,jarName);
                }
            }
            
        } catch (Exception e) {
            throw e;
        }
    }
    
    private void loadPlugins() throws Exception
    {
        SubnodeConfiguration plugins = iniConfig.getSection("Plugins");
        if (plugins.isEmpty())
            return;
        Iterator i = plugins.getKeys();
        while (i.hasNext()) {
            String resource = (String) i.next();

            if (resource != null)
            {
                resource = resource.replace("..",".");
            }
            File resourceFile = new File(resource);
            if (!resourceFile.exists())
            {
                logger.error("File does not exist:" + resource);
                continue;
            }
            String jarName = resource;
            logger.debug("Register:" + resource);
            pluginLoader.registerPlugin(resource);
        }
        
    }
    
    
    private void validate(String[] args) throws Exception
    {
        Map argMap = null;
        try
        {
            argMap = ArgsParser.getArgs(args, requiredArgs);
        }
        catch(Exception e)
        {
            throw e;
        }
        
        try
        {
            this.configProperties = this.getConfigFile();
        }
        catch(Exception e)
        {
            throw e;
        }
        logger.debug("Properties:" + configProperties);
        
        String tmp = (String)argMap.get("-binaryConfigFile");
        if (StringUtils.isBlank(tmp))
        {
            throw new Exception("Invalid binaryConfigFile");
        }
        
        try
        {
            File file = new File(tmp);
            if (!file.exists())
            {
                throw new Exception("binaryConfigFile does not exist");
            }
        }
        catch(Exception e)
        {
            throw e;
        }
        this.configFileName = tmp;
        // load the ini file.
        try
        {
            this.loadExportCategories();
        }
        catch(Exception e)
        {
            throw e;
        }
        
    }
    
    
    /**
     * load the ini file with the categories and the resources to export
     * @throws Exception 
     */
    private void loadExportCategories() throws Exception
    {
        WebConfigLoader loader = new WebConfigLoader();
        iniConfig = new INIConfiguration();
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(this.configFileName));
            iniConfig.read(reader);
        }
        catch(Exception e)
        {
            logger.error("binaryConfigFile Failed to load");
            throw e;
        }
    }
}
