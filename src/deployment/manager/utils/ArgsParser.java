/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fforester
 */
public class ArgsParser {
    
    
    public static Map<String,String> getArgs(String[] args,String[] required)
    {
        Map<String,String> optsMap = new HashMap();
        
        for(int i=0;i<args.length;i++)
        {
            switch(args[i].charAt(0))
            {
                case '-':
                    if (args[i].length() < 2)
                    {
                        throw new IllegalArgumentException("Not Valid argument:" + args[i]);
                    }
                    if (args[i].charAt(1) == '-')
                    {
                        if (args[i].length() < 3)
                        {
                            throw new IllegalArgumentException("Not Valid argument:" + args[i]);
                        }
                    }
                    else
                    {
                        if (args.length - 1 == i)
                        {
                            throw new IllegalArgumentException("expected argument after:" + args[i]);
                        }
                        optsMap.put(args[i],args[i+1]);
                        i++;
                    }
                    break;
                default:
                    break;
            }
        }
        
        for(String r : required)
        {
            if (optsMap.containsKey(r))
                continue;
            throw new IllegalArgumentException("Missing required argument:" + r);
        }
        return optsMap;
    }
    
}
