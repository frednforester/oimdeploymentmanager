/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager.utils;

import Thor.API.Operations.tcExportOperationsIntf;
import Thor.API.Operations.tcImportOperationsIntf;
import com.thortech.xl.vo.ddm.ImportPlanInfo;
import com.thortech.xl.vo.ddm.RootObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import oracle.iam.platform.OIMClient;
import org.apache.log4j.Logger;

/**
 *
 * @author fforester
 */
public class DMHelper {
    
    private final Logger logger = Logger.getLogger(DMHelper.class);
    
    private final String[] processTypes = {"Process","EventHandler","Resource"};
    
    private final OIMDeploymentManager deployOps;
    private final tcImportOperationsIntf importOps;
    private final tcExportOperationsIntf exportOps;
    private String baseDir;
    

    public DMHelper(OIMClient client) throws Exception {
        deployOps = new OIMDeploymentManager(client);
        try
        {
            exportOps = client.getService(tcExportOperationsIntf.class);
            importOps = client.getService(tcImportOperationsIntf.class);
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    public void setBaseDir(String baseDir) {
        this.baseDir = baseDir;
        deployOps.setBaseDirectory(baseDir);
    }
    
    
    public void exportBasicCategories(String[] categories)
    {
        try
        {
            for(String category : categories)
            {
                logger.info("Export category:" + category);
                String dirname = this.baseDir + "/" + category;
                File newdir = new File(dirname);
                newdir.mkdir();
                deployOps.setBaseDirectory(dirname);
                deployOps.exportSingleObjects(category);
                //this.getClient().logout();
            }
        }
        catch(Exception e)
        {
            logger.error("APIError:" + e.getMessage(),e);
            return;
        }
    }
    
    public void exportUserMetadata() throws Exception
    {
        // export user meta data
        try
        {
            String dirname = this.baseDir + "/" + "UserMetadata";
            deployOps.setBaseDirectory(dirname);
            File newdir = new File(dirname);
            newdir.mkdir();
            logger.debug("getAllObjectsByType");
            Collection<RootObject> usermeta = deployOps.getAllDepsForResource("User Metadata","*");
            deployOps.exportObjects(usermeta, "UserMetadata");
        }
        catch(Exception e)
        {
            logger.error("APIError:" + e.getMessage());
            throw e;
        }
    }
    
    public Collection<RootObject> retrieveDependencyTree(Collection<RootObject> roots) throws Exception
    {
        try
        {
            Collection<RootObject> usermetaDeps = exportOps.retrieveDependencyTree(roots);
            usermetaDeps.addAll(roots);
            return usermetaDeps;
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    
    public Collection<RootObject> retrieveAll(Collection<RootObject> roots) throws Exception
    {
        try
        {
            Collection<RootObject> usermetaDeps = exportOps.getDependencies(roots);
            roots.addAll(usermetaDeps);
            usermetaDeps = exportOps.retrieveChildren(roots);
            roots.addAll(usermetaDeps);
            usermetaDeps = exportOps.retrieveDependencyTree(roots);
            roots.addAll(usermetaDeps);
            return roots;
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    public Collection<String> retrieveCategories() throws Exception
    {
        try
        {
            return this.exportOps.retrieveCategories();
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    public void exportAllJobs() 
    {
        String dirname = this.baseDir + "/" + "Job";
        deployOps.setBaseDirectory(dirname);
        File newdir = new File(dirname);
        newdir.mkdir();
        List<String> names;
        try {
            names = deployOps.getAllObjectNamesByType("scheduledTask");
           
            for (String name : names) 
            {
                logger.debug("Jobname:" + name);
                boolean hasTask = false;
                Collection<RootObject> all = new ArrayList();
                Collection<RootObject> roots = deployOps.getFullObjectTree(name,"scheduledTask");
                for(RootObject r : roots)
                {
                    logger.debug("ROOT:" + r.getName() + ":" + r.getPhysicalType());
                    if (!all.contains(r))
                    {
                        all.add(r);
                        if (r.getChilds() != null && r.getChilds().size() > 0)
                        {
                            logger.debug("has children:" + r.getChilds());
                            all.addAll(r.getChilds());

                        }
                        deployOps.exportObjects(all, r.getName());
                        break;
                    }
                }
                /*
                all.addAll(roots);
                Collection<RootObject> children = deployOps.getObjectChildren(roots);
                for(RootObject r : children)
                {
                    if (!all.contains(r))
                        all.add(r);
                }
                children = deployOps.getObjectDependencies(roots);
                for(RootObject r : children)
                {
                    if (!all.contains(r))
                        all.add(r);
                }
                for (RootObject r : all) 
                {
                    logger.debug("R:" + r.getName() + ":" + r.getPhysicalType());
                    if (r.getPhysicalType().equals("Job"))
                        hasTask = true;
                }
                
                if (hasTask)
                {
                    //deployOps.exportObjects(all, name);
                }
                else
                    logger.error("Skipping missing task for job:" + name);
                */
            }
        } catch (Exception e) 
        {
            logger.error("APIError:" + e.getMessage());
            return;
        }
    }
    
    public void exportAllProcesses() 
    {
        List<String> processTypes = Arrays.asList(this.processTypes);
        String dirname = this.baseDir + "/" + "Process";
        deployOps.setBaseDirectory(dirname);
        File newdir = new File(dirname);
        newdir.mkdir();
        List<String> names;
        try {
            names = deployOps.getAllObjectNamesByType("Process");
            for (String name : names) 
            {
                Collection<RootObject> all = deployOps.getFullObjectTree(name, "Process");
                Iterator i = all.iterator();
                while(i.hasNext())
                {
                    RootObject r = (RootObject)i.next();
                    logger.debug("R:" + r.getName() + ":" + r.getPhysicalType());
                    String type = r.getPhysicalType();
                    if (!processTypes.contains(type))
                        i.remove();
                        
                }
                deployOps.exportObjects(all, name);
            }
        } catch (Exception e) 
        {
            logger.error("APIError:" + e.getMessage());
            return;
        }
    }
    
    public void exportAllProcessForms()
    {
        // export all process forms
        List<String> names;
        try
        {
            names = deployOps.getAllObjectNamesByType("Process Form");
            names = this.exportProcessForms(names);
            logger.info("Exporting Missed Forms:" + names);
            for(String name : names)
            {
                deployOps.exportByTypeAndName("Process Form", name);
            }
        }
        catch(Exception e)
        {
            logger.error("APIError:" + e.getMessage());
            return;
        }
    }
    
    public List<String> exportProcessForms(List<String> names) throws Exception
    {
        List<String> namesCopy = new ArrayList();
        namesCopy.addAll(names);
        String dirname = this.baseDir + "/" + "Process Form";
        deployOps.setBaseDirectory(dirname);
        File newdir = new File(dirname);
        newdir.mkdir();
        
        for(String name : names)
        {
            Collection<RootObject> roots = deployOps.getRootObject("Process Form", name);
            for(RootObject r : roots)
            {
                logger.debug("ROOT:" + r);
            }
            Collection<RootObject> children = deployOps.getObjectChildren(roots);
            
            boolean skip = false;
            for(RootObject r : children)
            {
                logger.debug("CHILD:" + r);
                if (r.getChilds().size() > 0)
                    logger.debug("Has Children:" + r.getName());
                else
                    skip = true;
            }
            
            if (skip)
            {
                logger.debug("Skipping:" + name);
                continue;
            }
            
            Collection<RootObject> allObjects = new ArrayList();
            allObjects.addAll(roots);
            allObjects.addAll(children);
            Collection<RootObject> deps = deployOps.getObjectDependencies(children);
            for(RootObject r : deps)
            {
                logger.debug("DEP:" + r);
                if (!allObjects.contains(r))
                    allObjects.add(r);
            }
            Collection<RootObject> tree = deployOps.getObjectDependencyTree(allObjects);
            Iterator i = tree.iterator();
            while(i.hasNext())
            {
                RootObject r = (RootObject)i.next();
                //logger.debug("TREE:" + r.getName() + ":" + r.getPhysicalType());
                if (!r.getPhysicalType().contains("Form"))
                    i.remove();
            }
            tree.addAll(roots);
            for(RootObject r : tree)
            {
                logger.debug("TREE:" + r.getName() + ":" + r.getPhysicalType());
                namesCopy.remove(r.getName());
            }
            logger.debug("Writing:" + name);
            deployOps.exportObjects(tree, name);
        }
        return namesCopy;
    }
    
    public Collection<RootObject> getRootObject(String type,String name) throws Exception
    {
        Collection<RootObject> roots;
        try {
            roots = deployOps.getRootObject(type, name);
            logger.debug("Objects:" + roots.size());
            return roots;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
    
    public void getLock() throws Exception
    {
        try
        {
            importOps.acquireLock(true);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    public void importBasicCategories(String[] categories) {
        for (String category : categories) {
            String catDir = this.baseDir + "/" + category;
            File folder = new File(catDir);
            File[] listOfFiles = folder.listFiles();

            for (File file : listOfFiles) {
                if (!file.isFile()) {
                    continue;
                }
                try {
                    logger.debug("File:" + file.getAbsolutePath());
                    String content = readFile(file.getAbsolutePath());
                    importFile(file.getAbsolutePath(), content);
                } catch (Exception e) {
                    logger.error("APIError:" + e.getMessage(),e);
                    break;
                }
            }
        }
    }
    
    public void exportObjects(Collection<RootObject> objects,String desc) throws Exception
    {
        try
        {
            deployOps.exportObjects(objects, desc);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    private String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                //stringBuilder.append(ls);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }
    
    public void importFile(String fileName) throws Exception
    {
        try
        {
            File file = new File(fileName);
            String content = readFile(file.getAbsolutePath());
            importFile(file.getAbsolutePath(), content);
            
        }
        catch(Exception e)
        {
            logger.error("APIError:" + e.getMessage());
            throw e;
        }
    }
    
    public void importFile(String fileName,String contents) throws Exception
    {
        
        try
        {
            Long msl = System.currentTimeMillis();
            String ms = msl.toString();
            Collection<RootObject> justImported = importOps.addXMLFile(fileName, contents);
            for(RootObject r : justImported)
            {
                logger.debug("Resource in file:" + r);
                    
            }
            Collection<RootObject> subs = importOps.listPossibleSubstitutions(justImported);
            logger.debug("Possible Substitution:" + subs);
            for(RootObject r : subs)
            {
                logger.debug(r.getName() + ":" + r.getPhysicalType());
                if (r.getPhysicalType().contains("Version"))
                    importOps.addSubstitution(r, ms);
            }
            
            HashMap mapsubs = importOps.getSubstitutions();
            logger.debug("Generated Substitutions:" + mapsubs);
            
            Collection<RootObject> missing = importOps.getMissingDependencies(justImported, "*");
            for(RootObject r:missing)
                logger.warn("Missing:" + r.getName() + ":" + r.getPhysicalType());
            
            HashMap messages = importOps.getImportMessages(justImported);
            Set keys = messages.keySet();
            boolean hasErrors = false;
            for(Object k : keys)
            {
                HashSet hs = (HashSet)messages.get(k);
                Iterator i = hs.iterator();
                while(i.hasNext())
                {
                    ImportPlanInfo o = (ImportPlanInfo)i.next();
                    if (o.getLevel() > 0 && !o.getMessageID().contains("RECENTTARGET"))
                    {
                        hasErrors = true;
                        logger.error("    ERROR:" +  o.getLevel() + ":" +  o.getMessageID() + ":" + o.getMessage() + ":" + o.getAdditionalInfo());
                    }
                }
                //logger.debug("MSGID:" + k + "MSGDESC:" + messages.get(k));
                //logger.debug("CLASS:" + messages.get(k).getClass());
                //logger.debug("CLASS:" + k.getClass());
            }
            
            if (!hasErrors)
            {
                logger.info("Importing:" + fileName);
                importOps.performImport(justImported);
            }
            else
            {
                logger.error("Skipping for errors:" + fileName);
            }
        }
        catch(Exception e)
        {
            logger.error("APIError:" + e.getMessage());
            throw e;
        }
    }
}
