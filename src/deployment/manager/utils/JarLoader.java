/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager.utils;

import com.thortech.util.logging.Logger;
import java.util.Set;
import oracle.iam.platform.OIMClient;
import oracle.iam.platformservice.api.PlatformUtilsService;
import oracle.iam.platformservice.vo.JarElement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author fforester
 */
public class JarLoader {

    private static final Logger logger = Logger.getLogger(JarLoader.class.getName());
    private PlatformUtilsService utilsService;

    public JarLoader(OIMClient client) throws Exception {
        
        try
        {
            utilsService = client.getService(PlatformUtilsService.class);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    public void loadJar(String jarType,String jarPath,String jarName) throws Exception 
    {
        
        if (StringUtils.isEmpty(jarPath))
        {
            throw new RuntimeException("Invalid Jar Path");
        }
        
        if (StringUtils.isEmpty(jarType))
        {
            throw new RuntimeException("Invalid Jar Type");
        }
        
        if (StringUtils.isEmpty(jarName))
        {
            throw new RuntimeException("Invalid Jar Name");
        }
        
        Set<JarElement> elems = MakeJarElement.getElement(jarName,jarType,jarPath);
        if (elems == null)
        {
            throw new RuntimeException("Invalid Jar Check Type,Name and Location");
        }
        /*
        try
        {
            utilsService.deleteJars(elems);
        }
        catch(Exception e)
        {
            if (!ignoreDelete)
                throw new RuntimeException("Error Deleting Jar",e);
        }
        */
        try
        {
            utilsService.uploadJars(elems);
        }
        catch(Exception e)
        {
            throw new RuntimeException("Error Uploading Jar",e);
        }
        /*
        try
        {
            utilsService.purgeCache("All");
        }
        catch(Exception e)
        {
            throw new RuntimeException("Error Purging Cache",e);
        }
        */
        logger.info("Reload Complete");
        
          
    }

}
