/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deployment.manager.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import oracle.iam.platform.OIMClient;
import oracle.iam.platformservice.api.PlatformService;
import org.apache.log4j.Logger;

/**
 *
 * @author fforester
 */
public class PluginLoader {
    
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private PlatformService platformService;

    public PluginLoader(OIMClient client) {
        
        try
        {
            platformService = client.getService(PlatformService.class);
        }
        catch(Exception e)
        {
            throw e;
        }
    }
    
    

    public void registerPlugin(String zipFilePath)  throws Exception {
        
        if (zipFilePath == null || zipFilePath.trim().length() == 0)
        {
            throw new RuntimeException("Specify a file path");
        }
        int fileSize = 0;
        FileInputStream fis;
        
        try
        {
            File file = new File(zipFilePath);
            fileSize = (int) file.length();
            if (fileSize == 0)
            {
                throw new Exception("File is Empty");
            }
            fis = new FileInputStream(file);
        }
        catch(FileNotFoundException e)
        {
            throw new Exception(e.getMessage());
        }
        catch(Exception e)
        {
            throw e;
        }
        
        byte[] b = new byte[fileSize];
        
        try
        {
            int bytesRead = fis.read(b, 0, fileSize);
            while (bytesRead < fileSize) {
                bytesRead += fis.read(b, bytesRead, fileSize - bytesRead);
            }
            fis.close();
        }
        catch(IOException e)
        {
            throw e;
        }
        
        try
        {
            this.platformService.registerPlugin(b);
        }
        catch(Exception e)
        {
            throw e;
        }
        
    }
    
}
